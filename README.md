CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
-------------
Custom Checkboxes module allows users to style checkboxes in a fast way.
Users only have to adapt the CSS a little.

REQUIREMENTS
-------------
No special requirements.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------
 * To use this module, you have to insert the library named "custom_checkboxes"
   as follows:

   {{ attach_library('custom_checkboxes/custom_checkboxes') }}

MAINTAINERS
-----------
 * Roger Codina - https://www.drupal.org/u/rcodina
 * Santi López - https://www.drupal.org/u/santilopez
